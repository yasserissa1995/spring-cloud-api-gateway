package com.yasser.gateway.controller.vm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.cloud.client.ServiceInstance;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RouteVM {

   private String path;

   private String serviceId;

   private List<ServiceInstance> serviceInstances = new ArrayList<>();

}
