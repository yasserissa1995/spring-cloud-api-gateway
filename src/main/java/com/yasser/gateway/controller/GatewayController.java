package com.yasser.gateway.controller;

import com.yasser.gateway.controller.vm.RouteVM;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class GatewayController {

   private final RouteLocator routeLocator;
   private final DiscoveryClient discoveryClient;


   @GetMapping("/routes")
   public ResponseEntity<List<RouteVM>> activeRoutes() {
      log.info("Rest request to get routes");

      Flux<Route> routes = routeLocator.getRoutes();
      List<RouteVM> routeVMs = new ArrayList<>();

      routes.subscribe(route -> {

         RouteVM routeVM = new RouteVM();
         // Manipulate strings to make Gateway routes look like Zuul's
         String predicate = route.getPredicate().toString();
         String path = predicate.substring(predicate.indexOf("[") + 1, predicate.indexOf("]"));
         routeVM.setPath(path);
         String serviceId = route.getId().substring(route.getId().indexOf("_") + 1).toLowerCase();
         routeVM.setServiceId(serviceId);
         // Exclude gateway app from routes
         if (!serviceId.equalsIgnoreCase("gateway")) {
            routeVM.setServiceInstances(discoveryClient.getInstances(serviceId));
            routeVMs.add(routeVM);
         }

      });

      return ResponseEntity.ok(routeVMs);
   }
}